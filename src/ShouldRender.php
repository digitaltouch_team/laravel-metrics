<?php

namespace DigitalMindset\Metrics;

interface ShouldRender
{
    public function component();
}
