<?php

namespace DigitalMindset\Metrics;

interface ShouldSpanCols
{
    public function colSpan();
}
