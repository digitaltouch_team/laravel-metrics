<?php

namespace DigitalMindset\Metrics;

interface ShouldCache
{
    public function cacheFor();
}
