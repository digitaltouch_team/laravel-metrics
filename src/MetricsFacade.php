<?php

namespace DigitalMindset\Metrics;

use Illuminate\Support\Facades\Facade;

/**
 * @see \DigitalMindset\Metrics\MetricsRepository
 */
class MetricsFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'laravel-metrics';
    }
}
