<?php

namespace DigitalMindset\Metrics;

final class MetricsRepository
{
    protected array $metrics = [];

    public function add(Metric $metric): MetricsRepository
    {
        array_push($this->metrics, $metric);

        return $this;
    }

    public function metrics(): array
    {
        return $this->metrics;
    }
}
